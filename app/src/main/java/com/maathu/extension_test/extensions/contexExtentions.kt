package com.maathu.extension_test.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent

inline fun <reified T : Activity> Context?.startActivity(): String? = this?.startActivity(Intent(this, T::class.java))