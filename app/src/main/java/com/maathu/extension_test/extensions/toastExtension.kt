package com.maathu.extension_test.extensions

import android.content.Context
import android.widget.Toast

fun Context?.toastLong(text: String, duration: Int = Toast.LENGTH_LONG) = this?.let { Toast.makeText(it, text, duration).show() }