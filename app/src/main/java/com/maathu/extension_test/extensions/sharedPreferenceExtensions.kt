package com.maathu.extension_test.extensions

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.maathu.extension_test.model.User


fun SharedPreferences.saveUser(user: User){

   //set variables of 'user'

     val editor = this.edit();
    var gson = Gson();
    var json = gson.toJson(user);
    editor.putString("MyObject", json);
    editor.apply();
}

fun SharedPreferences.getUser(context: Context): User{

 var editor = this.edit();
 val gson = Gson()
 var json: String? = this.getString("MyObject", "")
 val obj: User = gson.fromJson(json, User::class.java)

 return obj
}