package com.maathu.extension_test.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User (
    var fullName: String? = null,
    var email: String? = null,
    var phone: Int? = null,
    var gender: Boolean? = false,
    var address: String? = null
): Parcelable