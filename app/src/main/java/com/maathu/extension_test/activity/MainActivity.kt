package com.maathu.extension_test.activity

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.RadioButton
import com.maathu.extension_test.R
import com.maathu.extension_test.extensions.*
import com.maathu.extension_test.model.User
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {
    private var user = User()
    private lateinit var name: EditText
    private lateinit var email: EditText
    private lateinit var phone: EditText
    private lateinit var male: RadioButton
    private lateinit var feMale: RadioButton
    private lateinit var address: EditText
    private val sharedPreferences by inject<SharedPreferences>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


          name = findViewById<EditText>(R.id.et_name)
          email = findViewById<EditText>(R.id.et_email)
          phone = findViewById<EditText>(R.id.et_phone)
          male = findViewById<RadioButton>(R.id.male)
          feMale = findViewById<RadioButton>(R.id.female)
          address = findViewById<EditText>(R.id.et_address)

        openDetailActivity()
    }


    private fun openDetailActivity(){
        user.fullName = name.toString()
        var emailCheck = email.toString()

        if (emailCheck.isValidEmail()){
            user.email = email.toString()

            val phoneNumber = phone.toString()
            if (phoneNumber.isValidPhone()){
                user.phone =  phone.toString().toInt()

                if (male.isChecked || feMale.isChecked){
                    if (male.isChecked){
                        user.gender = false
                    }else{
                        user.gender = false
                    }

                    user.address = address.toString()
                    sharedPreferences.saveUser(user)
                }else{
                    toastLong("Please Select Gender")
                }

            }else{
                toastLong("enter Valid phone NUmber")
            }

        }else{
            toastLong("Enter valid Email")
        }




        startActivity<DetailsActivity1>()
    }
}