package com.maathu.extension_test.activity

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import com.maathu.extension_test.R
import com.maathu.extension_test.extensions.getUser
import com.maathu.extension_test.model.User
import org.koin.android.ext.android.inject

class DetailsActivity1 : AppCompatActivity() {
    private val sharedPreferences by inject<SharedPreferences>()
    private var user = User()
    private lateinit var name: TextView
    private lateinit var email: TextView
    private lateinit var phone: TextView
    private lateinit var gender: TextView
    private lateinit var address: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details1)

        user = sharedPreferences.getUser(this)

        if (user.gender == true){
            gender.text = "FeMale"
        }else{
            gender.text = "Male"
        }
        name.text = user.fullName
        email.text = user.email
        phone.text = user.phone.toString()
        address.text = user.address
    }
}